package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.List;





public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private List<Service> services;
	
	public void loadServices (String serviceFile) {
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with " + serviceFile);

		this.services = new List<>();

		JSONParser parser = new JSONParser();
		JSONObject jsonObject;
		String aux;
		Service service;
		String tripId;
		String taxiId;
		String company;
		int dropoffCommunityArea;
		int pickupCommunityArea;
		int tripSeconds;
		double tripMiles;
		double tripTotal;

		try {
			Object obj = parser.parse(new FileReader(serviceFile));
			JSONArray jsonArray = (JSONArray) obj;
			Iterator<JSONObject> iterator = jsonArray.iterator();
			while(iterator.hasNext()) {
				jsonObject = (JSONObject)iterator.next();
				tripId = (String) jsonObject.get("trip_id");
				taxiId = (String) jsonObject.get("taxi_id");
				company = (String) jsonObject.get("company");
				aux = (String) jsonObject.get("dropoff_community_area");
				if(aux!= null) {
					dropoffCommunityArea = Integer.parseInt(aux);
				}else {
					dropoffCommunityArea = 0;
				}
				aux = (String) jsonObject.get("pickup_community_area");
				if(aux!=null) {
					pickupCommunityArea = Integer.parseInt(aux);
				}else {
					pickupCommunityArea = 0;
				}
				aux = (String) jsonObject.get("trip_seconds");
				tripSeconds = Integer.parseInt(aux);
				aux = (String) jsonObject.get("trip_miles");
				tripMiles = Double.parseDouble(aux);
				aux = (String) jsonObject.get("trip_total");
				tripTotal = Double.parseDouble(aux);
				service = new Service(tripId, taxiId, company,dropoffCommunityArea, pickupCommunityArea,tripSeconds, tripMiles, tripTotal);
				this.services.add(service);
			}
			Iterator it = services.iterator();
			while(it.hasNext()) {
				System.out.println(it.next().toString());
				System.out.println();
			}
			this.services = services;
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}catch(IOException e) {
			e.printStackTrace();
		}catch(ParseException e) {
			e.printStackTrace();
		} catch(Exception e){
			e.printStackTrace();
		}

	}

	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		List<Service> taxiServicesToCommunityArea = new List<>();
		Iterator<Service> it = this.services.iterator();
		Service service;
		while(it.hasNext()) {
			service = it.next();
			if(service.getDropoffCommunityArea() == communityArea) {
				taxiServicesToCommunityArea.add(service);
			}
		}
		return taxiServicesToCommunityArea;
	}
	
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxisOfCompany with " + company);
		List<Taxi> taxis = new List<>();
		Taxi taxi;
		String taxiId;
		String comp;
		Service service;
		Iterator<Service> iterator = this.services.iterator();
		while(iterator.hasNext()) {
			service = iterator.next();
			comp = service.getCompany();

			if(comp!=null) {
				if(comp.equals(company)) {
					taxiId = service.getTaxiId();
					taxi = new Taxi(taxiId, company);
					if(taxis.get(taxi) == null) {
						taxis.add(taxi);
					}

				}
			}
		}
		return taxis;
	}


}
