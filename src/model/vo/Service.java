package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	private String tripId;
	private String taxiId;
	private String company;
	private int dropoffCommunityArea;
	private int pickupCommunityArea;
	private int tripSeconds;
	private double tripMiles;
	private double tripTotal;
	
	public Service(String tripId, String taxiId, String company, int dropoffCommunityArea, int pickupCommunityArea,
			int tripSeconds, double tripMiles, double tripTotal) {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return this.tripId;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return this.taxiId;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return this.tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return this.tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return this.tripTotal;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getCompany() {
		// TODO Auto-generated method stub
		return this.company;
	}

	public int getDropoffCommunityArea() {
		// TODO Auto-generated method stub
		return this.dropoffCommunityArea;
	}
}
