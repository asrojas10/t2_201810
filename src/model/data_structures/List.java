package model.data_structures;

import java.util.Iterator;
import model.vo.Service;

public class List <T extends Comparable<T>> implements LinkedList<T>, Iterable<T> {

	private Nodo head;
	private Nodo last;
	private int size;
	private Nodo current;
	
	public List ()
	{
		this.head=null;
		this.size=0;
	}
	public class Nodo <T extends Comparable<T>> {
		T item;
		Nodo next;
		
		public Nodo() {
			this.item = null;
			this.next = null;
		}
		public Nodo (T item) {
			this.item = item;
			this.next = null;
		}

		public T getItem() {
			return item;
		}

		public void setItem(T item) {
			this.item = item;
		}

		public Nodo getNext() {
			return next;
		}

		public void setNext(Nodo next) {
			this.next = next;
		}
		
	}
	
	public void add(T item) {
		
		Nodo <T> nuevo = new Nodo <T>(item);
		
		if (this.head==null)
		{
			this.head=nuevo;
			this.last=nuevo;
			this.size++;
		}
		else
		{
			Nodo <T> ultimo = this.last;
			ultimo.setNext(nuevo);
			this.last=nuevo;
			this.size++;
		}
		
	}


	public void delete(T item) 
	{
		if (this.head!=null)
		{
			Nodo <T> inicio = this.head;
			if (inicio.getItem()==item)
			{
				this.head= inicio.getNext();
				inicio=null;
				if(this.head.getNext()==null)
				{
					this.last=this.head;
				}
				this.size--;
			}
			else
			{
				while (inicio.getNext()!=null)
				{
					if(inicio.getNext().getNext()==item)
					{
						Nodo <T> temporal = inicio.getNext();
						Nodo <T> siguiente = temporal.getNext();
						temporal=null;
						inicio.setNext(siguiente);
						if (siguiente==null)
						{
							this.last= inicio;
						}
						this.size--;
						break;
					}
					inicio = inicio.getNext();
				}
			}
		}
		
	}


	public T get(T item) {
		Nodo <T> x = this.head;
		while (x!=null)
		{
			if (x.getItem().compareTo(item)==0)
			{
				return x.getItem();
			}
			x=x.getNext();
		}
		return null;
	}


	public int size() {
		
		return this.size;
	}

	@Override
	public T get(int pos) {
		T data = null;
		int cont = 0;
		
		Nodo <T> aux = this.head;
		
		while(aux !=null) {
			if(cont == pos) {
				data = aux.getItem();
				return data;
			}
			aux = aux.getNext();
			cont++;
		}
		return data;
	}

	@Override
	public void listing() {
		this.current=this.head;
	}

	@Override
	public T getCurrent() {
		
		return (T) this.current.getItem();
	}

	@Override
	public T next() {
		this.current=this.current.getNext();
		
		return (T) this.current.getItem();
	}


	public Iterator<T> iterator() {
		
		return new ListIterator();
	}
	private class ListIterator implements Iterator<T>{
		private Nodo current = head;

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return current != null;
		}

		@Override
		public T next() {
			// TODO Auto-generated method stub
			
			T item = (T) current.getItem();
			current = current.getNext();
			return item;
		}
	}


}
